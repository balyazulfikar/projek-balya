import React from "react";
import Image from "next/image";
import icon from "../img/icon.png";
import bg from "../img/login.png";
import Sunny from "../img/sun.png";
import english from "../img/Englishs.jpg";
import Form_login from "./Form_login";
function Login() {
  return (
    <div className="flex flex-row w-screen h-screen backdrop-contrast-200 " style={{ background: "#3C3E4A" }}>
      <div
        className="basis-7/12 w-full bg-fixed bg-no-repeat h-screen flex "
        style={{
          backgroundImage: `url(${bg.src})`,
          width: "100%",
          height: "100%",
        }}
      >
        <div className="grid w-full place-content-center">
          <Image src={icon} alt="img" className="object-contain h-48 w-96" />
        </div>
      </div>
      <div className="basis-2/5 w-full h-auto ">
        <div className="grid w-full h-auto justify-center">
          <div className="flex w-full flex-row  justify-center mt-10 mb-32 ">
            <div className="dropdown relative">
              <button
                style={{ background: "#505267" }}
                className="
                          dropdown-toggle
                         px-4
                          py-4
                          text-white
                          font-extralight
                          text-sm
                          leading-tight
                          uppercase
                          rounded-2xl
                          
                          
                          hover:bg-blue-100 hover:shadow-lg
                          focus:bg-blue-100 focus:shadow-lg focus:outline-none focus:ring-0
                          active:bg-blue-100  active:text-white
                          transition
                          duration-150
                          ease-in-out
                          flex
                          items-center
                          whitespace-nowrap
                        "
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <Image src={english} alt="img" sizes="35" className="mr-1" />
                English
                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="caret-down" className="w-2 ml-2" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                  <path fill="currentColor" d="M31.3 192h257.3c17.8 0 26.7 21.5 14.1 34.1L174.1 354.8c-7.8 7.8-20.5 7.8-28.3 0L17.2 226.1C4.6 213.5 13.5 192 31.3 192z"></path>
                </svg>
              </button>
              <ul
                className="
                          dropdown-menu
                          min-w-max
                          absolute
                          hidden
                          bg-white
                          text-base
                          z-50
                          float-left
                          py-2
                          list-none
                          text-left
                          rounded-lg
                          shadow-lg
                          mt-1
                          hidden
                          m-0
                          bg-clip-padding
                          border-none
                        "
                aria-labelledby="dropdownMenuButton1"
              >
                <li>
                  <a
                    className="
                                dropdown-item
                                text-sm
                                py-2
                                px-4
                                font-normal
                                block
                                w-full
                                whitespace-nowrap
                                bg-transparent
                                text-gray-700
                                hover:bg-gray-100
                              "
                    href="#"
                  >
                    Action
                  </a>
                </li>
                <li>
                  <a
                    className="
                                dropdown-item
                                text-sm
                                py-2
                                px-4
                                font-normal
                                block
                                w-full
                                whitespace-nowrap
                                bg-transparent
                                text-gray-700
                                hover:bg-gray-100
                              "
                    href="#"
                  >
                    Another action
                  </a>
                </li>
                <li>
                  <a
                    className="
                                dropdown-item
                                text-sm
                                py-2
                                px-4
                                font-normal
                                block
                                w-full
                                whitespace-nowrap
                                bg-transparent
                                text-gray-700
                                hover:bg-gray-100
                              "
                    href="#"
                  >
                    Something else here
                  </a>
                </li>
              </ul>
            </div>
            <div className="flex  space-x-2 justify-center ml-2 ">
              <button
                style={{ background: "#505267" }}
                type="button"
                className="inline-block px-4 py-3 
               shadow-md
                          bg-blue-100
                          text-white
                          font-extralight
                          text-xs
                          leading-tight
                          uppercase
                          rounded-xl
                          
                          
                          hover:bg-blue-100 hover:shadow-lg hover:text-blue-500
                          focus:bg-blue-100 focus:shadow-lg focus:outline-none focus:ring-0
                          active:bg-blue-100  active:text-white
                          transition
                          duration-150
                          ease-in-out
                          
                          items-center
                          whitespace-nowrap"
              >
                <Image src={Sunny} alt="sdasd" />
              </button>
            </div>
          </div>
          <Form_login />
        </div>
      </div>
    </div>
  );
}

export default Login;
