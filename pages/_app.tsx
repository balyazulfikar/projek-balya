import "../styles/globals.css";
import type { AppProps } from "next/app";
import { useEffect } from "react";

import "tw-elements/dist/css/index.min.css";

export default function App({ Component, pageProps }: AppProps) {
  useEffect(() => {
    import("tw-elements");
    import("tw-elements/dist/css/index.min.css");
  }, []);
  return <Component {...pageProps} className="app-wrapper" />;
}
