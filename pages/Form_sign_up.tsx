import React from "react";
import Image from "next/image";
import icon from "../img/icon.png";
import bg from "../img/login.png";
import Apple from "../img/Apple.png";
import Google from "../img/Google.png";
import Facebook from "../img/Facebook.png";
import Sunny from "../img/sun.png";
function SignUp() {
  return (
    <div className="flex flex-col h-screen justify-center items-center  text-white">
      <p className="text-xl">Create an Account</p>
      <h5 className="text-sm">it's free and takes less than </h5>
      <p className="text-sm">a minute.</p>
      <div className="block p-2  max-w-sm h">
        <form>
          <div className="form-group mb-6">
            <input
              style={{ background: "#D9D9D9" }}
              type="text"
              className="form-control rounded-xl
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        border border-solid border-gray-300
        transition
        ease-in-out
        m-0
        focus:border-r-white focus:outline-none"
              id="exampleInputEmail2"
              aria-describedby="emailHelp"
              placeholder="Full Name"
            />
          </div>
          <div className="form-group mb-6">
            <input
              style={{ background: "#D9D9D9" }}
              type="email"
              className="form-control rounded-xl
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        border border-solid border-gray-300
        transition
        ease-in-out
        m-0
        focus:border-r-white focus:outline-none"
              id="exampleInputEmail2"
              aria-describedby="emailHelp"
              placeholder="Your Email"
            />
          </div>
          <div className="form-group mb-6">
            <input
              style={{ background: "#D9D9D9" }}
              type="password"
              className="form-control rounded-xl
                    block
                    w-full
                    px-3
                    py-1.5
                    text-base
                    font-normal
                    text-gray-700
                    border border-solid border-gray-300
                    transition
                    ease-in-out
                    m-0
                    focus:border-r-white focus:outline-none"
              id="exampleInputEmail2"
              aria-describedby="emailHelp"
              placeholder="Password"
            />
          </div>

          <div className="flex flex-col justify-between items-center mb-6">
            <p className="form-check-label inline-block text-sm text-white" typeof="exampleCheck2">
              By submitting this form,your agree with our
            </p>

            <p className="mr-2 text-sm">
              <a href="#!" className=" mr-2 text-sm text-blue-400 hover:text-blue-500 focus:text-blue-700 transition duration-200 ease-in-out">
                Terms
              </a>
              and
              <a href="#!" className="ml-2 text-blue-400 text-sm hover:text-blue-500 focus:text-blue-700 transition duration-200 ease-in-out">
                Privicy policy
              </a>
            </p>
          </div>
          <button
            style={{ background: "#A89870" }}
            type="submit"
            className="
                              w-full
                              px-6
                              py-2.5
                            
                              text-black
                              font-medium
                              text-xs
                              leading-tight
                              uppercase
                              
                              shadow-md
                              hover:bg-blue-700 hover:shadow-lg hover:text-white
                              focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0
                              active:bg-blue-800 active:shadow-lg
                              transition
                              duration-150
                              ease-in-out ml-2
                              rounded-xl"
          >
            Create Account
          </button>
          <p className="text-white mt-6 text-center text-sm justify-between flex flex-row">
            do you already have a user account?
            <a href="/login" className="ml-2 text-blue-300 hover:text-blue-500 focus:text-blue-700 transition duration-200 ease-in-out">
              Login here
            </a>
          </p>
          <p className="text-white mt-6 text-center mb-3 text-sm">Or create an account with</p>
        </form>
        <div className="flex flex-row justify-around mr-3">
          <Image src={Facebook} alt="goog" />
          <Image src={Google} alt="goog" />
          <Image src={Apple} alt="goog" />
        </div>
      </div>
    </div>
  );
}

export default SignUp;
