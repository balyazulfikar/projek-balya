import React from "react";
import group from "../img/group.png";
import Apple from "../img/Apple.png";
import Google from "../img/Google.png";
import Facebook from "../img/Facebook.png";

import Image from "next/image";

function Form_login() {
  return (
    <div className="grid w-full   place-items-start overflow-x-auto pr-24">
      <div className="flex flex-col h-auto overflow-x-auto justify-center items-center text-white ">
        <p>Get into your club</p>
        <Image src={group} alt="group" />
        <div className="block p-6  max-w-sm">
          <form>
            <div className="form-group mb-6">
              <input
                style={{ background: "#D9D9D9" }}
                type="email"
                className="form-control rounded-xl
                        block
                        w-full
                        px-3
                        py-1.5
                        text-base
                        font-normal
                       text-gray-700
                        transition
                        ease-in-out
                        m-0
                       focus:border-r-white focus:outline-none"
                id="exampleInputEmail2"
                aria-describedby="emailHelp"
                placeholder="Email Adress"
              />
            </div>
            <div className="form-group mb-6">
              <input
                style={{ background: "#D9D9D9" }}
                type="password"
                className="form-control block
                         rounded-xl
                         w-full
                         px-3
                         py-1.5
                         text-base
                         font-normal
                       text-gray-700 
                         border border-solid border-gray-300
                         transition
                         ease-in-out
                         m-0
                       focus:border-r-white focus:outline-none"
                id="exampleInputPassword2"
                placeholder="Password"
              />
            </div>
            <div className="flex justify-between items-center mb-6">
              <div className="form-group form-check">
                <input
                  type="checkbox"
                  className="form-check-input   appearance-none h-4 w-4 border border-white rounded-sm checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                  id="exampleCheck2"
                />
                <label className="form-check-label inline-block text-white" typeof="exampleCheck2">
                  Remember me
                </label>
              </div>
              <a href="#!" className="ml-12  text-blue-300 hover:text-blue-500 focus:text-blue-700 transition duration-200 ease-in-out">
                Forgot password?
              </a>
            </div>
            <button
              style={{ background: "#A89870" }}
              type="submit"
              className="
                      w-full
                      px-6
                      py-2.5
                    
                      text-black
                      font-medium
                      text-xs
                      leading-tight
                      uppercase
                      
                      shadow-md
                      hover:bg-blue-700 hover:shadow-lg hover:text-white
                      focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0
                      active:bg-blue-800 active:shadow-lg
                      transition
                      duration-150
                      ease-in-out ml-2
                      rounded-xl"
            >
              Log in
            </button>
            <p className="text-white mt-6 text-center justify-evenly flex flex-row">
              Not user account?
              <a href="/sign_up" className=" text-blue-300 hover:text-blue-500 focus:text-blue-700 transition duration-200 ease-in-out">
                Register here
              </a>
            </p>
            <p className="text-white mt-6 text-center mb-3 ">Or login with</p>
          </form>
          <div className="flex flex-row justify-around mr-3">
            <button className=" transition duration-400 ease-in-out hover:ring hover:ring-yellow-700 border-spacing-3 rounded-2xl ">
              <a href="/sign_up" className=" text-blue-300 hover:text-blue-500 ">
                <Image src={Facebook} alt="goog" />
              </a>
            </button>
            <button>
              <Image src={Google} alt="goog" />
            </button>
            <button>
              <Image src={Apple} alt="goog" />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Form_login;
