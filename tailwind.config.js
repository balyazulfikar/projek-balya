module.exports = {
  content: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}", "./src/**/*.{html,js}", "./node_modules/tw-elements/dist/js/**/*.js"],
  theme: {
    extend: {},
  },
  plugins: [require("postcss-import"), require("tailwindcss/nesting"), require("tailwindcss"), require("autoprefixer"), require("tw-elements/dist/plugin")],
};
